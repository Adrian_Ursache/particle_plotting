﻿using ParticlePlotting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace ParticlePlotting
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const UInt16 LeftTopX = 20;
        const UInt16 LeftTopY = 20;
        const UInt16 RightBottomX = 785;
        const UInt16 RightBottomY = 570;
        const UInt16 ParticleDrawingMargin = 0;

        bool bPointsPlotted = false;
        bool bStartedSimulation = false;
        bool bReloadSimulation = false;
        int timerMiliseconds = 1000;

        ParticleAnimation animation;
        DispatcherTimer timer;

        public MainWindow()
        {
            InitializeComponent();
            DrawCanvasEdge();

            labelMiliseconds.Content = "1000 ms";

            animation = new ParticleAnimation(int.Parse(textBoxDensity.Text),
                double.Parse(textBoxParticleOneSize.Text),
                double.Parse(textBoxParticleTwoSize.Text));

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(timerMiliseconds);
            timer.Tick += DrawingThread;
        }

        /*
        * TODO: Add description
        */
        private void DrawCanvasEdge()
        {
            Line objLine_1 = new Line();
            Line objLine_2 = new Line();
            Line objLine_3 = new Line();
            Line objLine_4 = new Line();

            objLine_1.Stroke = System.Windows.Media.Brushes.Black;
            objLine_1.Fill = System.Windows.Media.Brushes.Black;
            objLine_1.X1 = LeftTopX;
            objLine_1.Y1 = LeftTopY;
            objLine_1.X2 = RightBottomX;
            objLine_1.Y2 = LeftTopY;

            objLine_2.Stroke = System.Windows.Media.Brushes.Black;
            objLine_2.Fill = System.Windows.Media.Brushes.Black;
            objLine_2.X1 = RightBottomX;
            objLine_2.Y1 = LeftTopY;
            objLine_2.X2 = RightBottomX;
            objLine_2.Y2 = RightBottomY;

            objLine_3.Stroke = System.Windows.Media.Brushes.Black;
            objLine_3.Fill = System.Windows.Media.Brushes.Black;
            objLine_3.X1 = RightBottomX;
            objLine_3.Y1 = RightBottomY;
            objLine_3.X2 = LeftTopX;
            objLine_3.Y2 = RightBottomY;

            objLine_4.Stroke = System.Windows.Media.Brushes.Black;
            objLine_4.Fill = System.Windows.Media.Brushes.Black;
            objLine_4.X1 = LeftTopX;
            objLine_4.Y1 = RightBottomY;
            objLine_4.X2 = LeftTopX;
            objLine_4.Y2 = LeftTopY;

            MainCanvas.Children.Add(objLine_1);
            MainCanvas.Children.Add(objLine_2);
            MainCanvas.Children.Add(objLine_3);
            MainCanvas.Children.Add(objLine_4);
        }

        /*
        * TODO: Add description
        */
        private void DrawParticles()
        {
            MainCanvas.Children.Clear();
            if (!bPointsPlotted)
            {
                animation.CreateParticleList();
            }
            DrawCanvasEdge();
            foreach (Particle particle in animation.particleList)
            {
                MainCanvas.Children.Add(particle.particle);
                Canvas.SetLeft(particle.particle, particle.particleCoords.X + particle.radius);
                Canvas.SetTop(particle.particle, particle.particleCoords.Y + particle.radius);
            }
        }

        /*
        * TODO: Add description
        */
        private void buttonPlotParticles_Click(object sender, RoutedEventArgs e)
        {
            bPointsPlotted = true;
            animation.CreateParticleList();
            DrawParticles();
        }

        /*
        * TODO: Add description
        */
        private void buttonStartSimulation_Click(object sender, RoutedEventArgs e)
        {
            if (bPointsPlotted )
            {
                timer.Start();
                buttonStartSimulation.IsEnabled = false;
            }
        }

        /*
        * TODO: Add description
        */
        private void DrawingThread(object sender, EventArgs e)
        {
            try
            {
                animation.UpdateParticlePosition();
                DrawParticles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception tickThread : " + ex.Message);
            }
        }

        /*
        * TODO: Add description
        */
        private void sliderMilisecondsValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            labelMiliseconds.Content = (Math.Ceiling(10 - e.NewValue * 10 / 11) * 100).ToString() + " ms";
            timerMiliseconds = (int)Math.Ceiling(10 - e.NewValue * 10 / 11) * 100;
            timer.Interval = TimeSpan.FromMilliseconds(timerMiliseconds);
        }
    }
}
