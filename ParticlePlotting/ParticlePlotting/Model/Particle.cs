﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace ParticlePlotting.Model
{
    /*
    * TODO: Add description
    */
    class Particle
    {
        public Point particleCoords { get; }
        public Ellipse particle { get; set; }
        public double radius { get; }
        public bool particleType { get; }

        public Particle(Point particleCoords, Ellipse particle, double radius, bool particleType)
        {
            this.particleCoords = particleCoords;
            this.particle = particle;
            this.radius = radius;
            this.particleType = particleType;
        }
    }
}
