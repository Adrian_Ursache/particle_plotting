﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ParticlePlotting.Model
{
    /*
    * TODO: Add description
    */
    class ParticleAnimation
    {
        public List<Particle> particleList = new List<Particle>();

        const UInt16 LeftTopX = 20;
        const UInt16 LeftTopY = 20;
        const UInt16 RightBottomX = 785;
        const UInt16 RightBottomY = 570;
        const UInt16 ParticleDrawingMargin = 0;

        int density;
        double particleOneSize;
        double particleTwoSize;

        public ParticleAnimation(int density, double particleOneSize, double particleTwoSize)
        {
            this.density = density;
            this.particleOneSize = particleOneSize;
            this.particleTwoSize = particleTwoSize;
        }

        /*
        * TODO: Add description
        */
        public void UpdateParticlePosition()
        {
            List<Particle> newParticleList = new List<Particle>();

            // Implement Particle Interaction here
            for (int index = 0; index < particleList.Count; ++index)
            {
                /* Start Position Calculation */
                double newX;
                double newY;

                if (!particleList[index].particleType)
                {
                    newX = particleList[index].particleCoords.X + 3;
                    newY = particleList[index].particleCoords.Y;

                    if (newX + particleList[index].radius * 2 > RightBottomX)
                    {
                        newX = LeftTopX;
                    }
                }
                else
                {
                    newX = particleList[index].particleCoords.X - 3;
                    newY = particleList[index].particleCoords.Y;

                    if (newX < LeftTopX)
                    {
                        newX = RightBottomX - particleList[index].radius * 2;
                    }
                }
                /* End Position Calculation */

                newParticleList.Add(new Particle(new Point(newX, newY),
                    particleList[index].particle,
                    particleList[index].radius,
                    particleList[index].particleType));                
            }
            particleList = newParticleList;
        }

        /*
        * TODO: Add description
        */
        private bool CreateParticle(int index)
        {
            Ellipse newParticle = new Ellipse();
            Point particleCoords = new Point();
            Random randomNumber = new Random();
            double particleSize;
            bool particleType;

            if (index < density / 2)
            {
                newParticle.Stroke = Brushes.Black;
                particleSize = particleOneSize;
                particleType = false;
            }
            else
            {
                newParticle.Stroke = Brushes.Red;
                particleSize = particleTwoSize;
                particleType = true;
            }
            newParticle.StrokeThickness = 2;
            newParticle.Width = particleSize;
            newParticle.Height = particleSize;

            int counter = 0;
            while (counter++ < 10000)
            {
                bool foundParticle = true;
                particleCoords.X = randomNumber.Next(LeftTopX, RightBottomX - (int)(particleSize));
                particleCoords.Y = randomNumber.Next(LeftTopY, RightBottomY - (int)(particleSize));

                foreach (Particle particle in particleList)
                {
                    if (particle.radius + particleSize / 2 > Math.Sqrt(Math.Pow(particleCoords.X - particle.particleCoords.X, 2) + Math.Pow(particleCoords.Y - particle.particleCoords.Y, 2)) ||
                        particleCoords.X < LeftTopX || particleCoords.X + particleSize > RightBottomX ||
                        particleCoords.Y < LeftTopY || particleCoords.Y + particleSize > RightBottomY)
                    {
                        foundParticle = false;
                        break;
                    }
                }
                if (foundParticle)
                {
                    break;
                }
            }
            if (counter >= 10000)
            {
                return false;
            }

            particleList.Add(new Particle(particleCoords, newParticle, particleSize / 2, particleType));
            return true;
        }

        /*
        * TODO: Add description
        */
        public void CreateParticleList()
        {
            particleList.Clear();

            for (int index = 0; index < density; ++index)
            {
                if (!CreateParticle(index))
                {
                    MessageBox.Show("Could not create all particles, density too big!", "Density Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                }
            }
        }

    }
}
